import com.homework.BMICalculator.BMI;
import com.homework.BMICalculator.BMIController;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;

/**
 * BMIControllerBMIParamExceptionTest tests BMIController Exceptions.
 */

@RunWith(Parameterized.class)
public class BMIControllerBMIParamExceptionTest {

    @Parameterized.Parameters public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {55, 60, "centimeter"},
                {273, 60, "centimeter"},
                {80, 4, "centimeter"},
                {80, 401, "centimeter"},
                {2.9, 60, "meter"},
                {0.4, 70, "meter"}
        });
    }
    @Parameterized.Parameter(value = 0) public double BMIHeight;
    @Parameterized.Parameter(value = 1) public double BMIWeight;
    @Parameterized.Parameter(value = 2) public String unit;

    private static BMIController controller;

    @BeforeClass
    public static void setUpBMIController(){
        controller = new BMIController("meter");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfOutOfRangeValuesGiveExpectionsInBMIContoller() {
        controller.setUnit(unit);
        BMI myBMI = controller.getBMI(BMIHeight,BMIWeight);
    }



}
