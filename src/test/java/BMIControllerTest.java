import com.homework.BMICalculator.BMI;
import com.homework.BMICalculator.BMIController;
import com.homework.BMICalculator.BMIService;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *  Tests BMIController if it is working correctly.
 */

@RunWith(MockitoJUnitRunner.class)
public class BMIControllerTest {

    private  BMIService mockService;
    private  static BMIController controller;
    private  boolean mockInitialized = false;

    @BeforeClass
    public static void setUpController(){
        controller = new BMIController("meter");
    }

    @Before
    public void setUp() {
        if (!mockInitialized) {
            MockitoAnnotations.initMocks(this);
            mockInitialized = true;
        }

        mockService = mock(BMIService.class);
        controller.setService(mockService);
    }

    @Test
    public void testIfRightValuesGivesBackTheRightBMI() throws IllegalArgumentException {
        when(mockService.calculateBMI(180, 70)).thenReturn(new BMI(21.60, "Normal"));
        when(mockService.calculateBMI(1.80, 70)).thenReturn(new BMI(21.60, "Normal"));

        controller.setUnit("centimeter");
        BMI myBMICentimeter = controller.getBMI(180, 70);
        BMI calculatedBMICentimeter = mockService.calculateBMI(180, 70);

        controller.setUnit("meter");
        BMI myBMIMeter = controller.getBMI(1.80, 70);
        BMI calculatedBMIMeter = mockService.calculateBMI(1.80, 70);

        assertEquals(calculatedBMICentimeter, myBMICentimeter);
        assertEquals(calculatedBMIMeter, myBMIMeter);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testIfWrongBMIControllerConstructorParamterThrowsAnException(){
        BMIController controllerTest = new BMIController("metere");
    }
}
