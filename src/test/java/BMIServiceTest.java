import static org.junit.Assert.assertEquals;
import com.homework.BMICalculator.BMI;
import com.homework.BMICalculator.BMIService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;

/**
 * BMIServiceTest tests the returned BMI class.
 */

@RunWith(Parameterized.class)
public class BMIServiceTest {

    @Parameterized.Parameters public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {190, 52, 14.40, "Severe Thinness"},
                {205, 70, 16.66, "Moderate Thinness"},
                {200, 70, 17.50, "Mild Thinness"},
                {180, 60, 18.52, "Normal"},
                {156, 70, 28.76, "Overweight"},
                {100, 30.2, 30.2, "Obese Class I"},
                {120, 56, 38.89, "Obese Class II"},
                {140, 84, 42.86, "Obese Class III"}
        });
    }

    @Parameterized.Parameter(value = 0) public int BMIHeight;
    @Parameterized.Parameter(value = 1) public double BMIWeight;
    @Parameterized.Parameter(value = 2) public double actualBMIValue;
    @Parameterized.Parameter(value = 3) public String actualCategory;

    private static BMIService service;

    @BeforeClass
    public static void setUpBMIService(){
         service = new BMIService();
    }

    @Test
    public void testValueFromGetBMIWithValidValue(){
        BMI myBMI = service.calculateBMI(BMIHeight, BMIWeight);
        assertEquals(actualBMIValue, myBMI.getValue(), 0.01);
    }

    @Test
    public void testCategoryFromGetBMIWithValidValue(){
        BMI myBMI = service.calculateBMI(BMIHeight, BMIWeight);
        assertEquals(actualCategory, myBMI.getCategory());
    }
}
