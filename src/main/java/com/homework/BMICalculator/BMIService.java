package com.homework.BMICalculator;

/**
 * BMIService calculates the BMI value.
 */

public class BMIService  {

    public BMI calculateBMI(double height, double weight) {
        String category = "";
        double value = weight / Math.pow(height, 2) * 10000;

        if(value < 16){
            category = "Severe Thinness";
        } else if (value >= 16 && value < 17) {
            category = "Moderate Thinness";
        } else if (value >= 17 && value < 18.5) {
            category = "Mild Thinness";
        } else if (value >= 18.5 && value < 25) {
            category = "Normal";
        } else if (value >= 25 && value < 30) {
            category = "Overweight";
        } else if (value >= 30 && value < 35) {
            category = "Obese Class I";
        } else if (value >= 35 && value < 40) {
            category = "Obese Class II";
        } else if (value > 40) {
            category = "Obese Class III";
        }

        return new BMI(value, category);
    }
}
