package com.homework.BMICalculator;

/**
 * Interface of the API BMICalculator
 */

public interface CalculatorInterface {

    public BMI getBMI(double height, double weight);
}
