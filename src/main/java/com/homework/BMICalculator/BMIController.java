package com.homework.BMICalculator;

/**
 * BMIController receives user requests.
 * User can decide which unit (meter or centimeter) he wants to use.
 */

public class BMIController implements CalculatorInterface {

    private double height;
    private double weight;
    private BMI myBMI;
    private BMIService service;
    private String unit;

    public BMIController(String unit) {
        setUnit(unit);
        service = new BMIService();
    }

    public BMI getBMI(double height, double weight) {
        evaluate(height, weight);
        myBMI = service.calculateBMI(height, weight);
        return myBMI;
    }

    private void evaluate(double height, double weight) {

        switch (unit) {
            case "centimeter":
                if (height > 272) {
                    throw new IllegalArgumentException("The tallest man was 272cm! Give a smaller value!");
                } else if (height < 56) {
                    throw new IllegalArgumentException("The smallest man was 56cm! Give a larger value!");
                }
                this.height = height;
                break;

            case "meter":
                if (height > 2.72) {
                    throw new IllegalArgumentException("The tallest man was 2,72m! Give a smaller value!");
                } else if (height < 0.56) {
                    throw new IllegalArgumentException("The smallest man was 0,56m! Give a larger value!");
                }
                this.height = height * 100;
                break;
        }

        if (weight < 5) {
            throw new IllegalArgumentException("The weight smaller than 5kg is invalid! Give a larger value, weight must be given in kg!");
        } else if (weight > 400) {
            throw new IllegalArgumentException("The weight greater than 400kg is invalid! Give a smaller value, weight must be given in kg!");
        }
        this.weight = weight;
    }

    public void setUnit(String unit){
        if(unit.toLowerCase().equals("meter") || unit.toLowerCase().equals("centimeter")){
            this.unit = unit;
            return;
        }
        throw new IllegalArgumentException("meter vagy centimeter paraméter megadása szükséges");
    }

    public void setService(BMIService service) {
        this.service = service;
    }

}
