package com.homework.BMICalculator;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * BMI class that contains the value and the description of the category.
 */

public class BMI {

    private double value;
    private String category;

    public BMI(double value, String category){

        this.value = value;
        this.category = category;
    }

    public double getValue() {

        return BigDecimal.valueOf(value)
                .setScale(2, RoundingMode.HALF_UP)
                .doubleValue();
    }

    public String getCategory() {
        return category;
    }
}
